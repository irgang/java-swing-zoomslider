package de.fuhagen.sttp.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import org.junit.Before;
import org.junit.Test;

import de.fuhagen.sttp.gui.ZoomSlider;
import de.fuhagen.sttp.gui.ZoomSliderListener;

/**
 * Tests for {@link ZoomSlider}.
 * 
 * @author thomas
 *
 */
public class ZoomSliderTest {

    /**
     * Listener dummy object.
     * 
     * @author thomas
     *
     */
    class TestListener implements ZoomSliderListener {

        public double  value  = 0.0;
        public boolean action = false;

        @Override
        public void updateView(double zoom) {
            value = zoom;
        }

        @Override
        public void executeAction() {
            action = true;
        }
    }

    /**
     * 
     * 
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.ZoomSlider#ZoomSlider(int, int, boolean)}.
     */
    @Test
    public void testZoomSlider() {
        ZoomSlider zs = new ZoomSlider(200, 100, true);
        assertNotNull(zs);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.ZoomSlider#setValue(int)}.
     */
    @Test
    public void testSetValue() {
        ZoomSlider zs = new ZoomSlider(200, 100, true);

        TestListener listener = new TestListener();
        zs.addZoomListener(listener);

        zs.setValue(200);
        assertTrue("set value update value", zs.getValue() == 200);
        assertTrue("call listeners " + listener.value + " == 2.0",
                listener.value == 2.0);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.ZoomSlider#getPreferredSize()}.
     */
    @Test
    public void testGetPreferredSize() {
        ZoomSlider zs = new ZoomSlider(456, 123, true);
        Dimension size = zs.getPreferredSize();

        assertTrue("width", size.width == 456);
        assertTrue("height", size.height == 123);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.ZoomSlider#mouseDragged(java.awt.event.MouseEvent)}.
     */
    @Test
    public void testMouseDragged() {
        ZoomSlider zs = new ZoomSlider(250, 70, true);

        MouseEvent mouseStart = new MouseEvent(zs, 0, System
                .currentTimeMillis(), 0, 50, 30, 1, false);
        MouseEvent mouseDragRight = new MouseEvent(zs, 0, System
                .currentTimeMillis(), 0, 100, 30, 1, false);

        int value = zs.getValue();
        zs.mousePressed(mouseStart);
        zs.mouseDragged(mouseDragRight);
        assertTrue("drag right reduce zoom " + value + " < " + zs.getValue(),
                value < zs.getValue());

        MouseEvent mouseDragLeft = new MouseEvent(zs, 0, System
                .currentTimeMillis(), 0, 20, 30, 1, false);

        value = zs.getValue();
        zs.mousePressed(mouseStart);
        zs.mouseDragged(mouseDragLeft);
        assertTrue("drag left increase zoom " + value + " > " + zs.getValue(),
                value > zs.getValue());
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.ZoomSlider#mouseClicked(java.awt.event.MouseEvent)}.
     */
    @Test
    public void testMouseClicked() {
        ZoomSlider zs = new ZoomSlider(250, 70, true);

        int value = zs.getValue();
        MouseEvent left = new MouseEvent(zs, 0, System.currentTimeMillis(), 0,
                50, 30, 1, false);
        zs.mouseClicked(left);
        assertTrue("left reduce zoom, " + value + " < " + zs.getValue(),
                value > zs.getValue());

        value = zs.getValue();
        MouseEvent right = new MouseEvent(zs, 0, System.currentTimeMillis(), 0,
                200, 30, 1, false);
        zs.mouseClicked(right);
        assertTrue("right increase zoom", value < zs.getValue());

        TestListener listener = new TestListener();
        zs.addZoomListener(listener);
        MouseEvent two = new MouseEvent(zs, 0, System.currentTimeMillis(), 0,
                125, 30, 2, false);
        zs.mouseClicked(two);
        assertTrue("center", listener.action);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.gui.ZoomSlider#mouseWheelMoved(java.awt.event.MouseWheelEvent)}.
     */
    @Test
    public void testMouseWheelMoved() {
        ZoomSlider zs = new ZoomSlider(250, 70, true);

        MouseWheelEvent wheelDown = new MouseWheelEvent(zs, 0, System
                .currentTimeMillis(), 0, 100, 30, 0, false,
                MouseWheelEvent.WHEEL_UNIT_SCROLL, 20, 20);
        MouseWheelEvent wheelUp = new MouseWheelEvent(zs, 0, System
                .currentTimeMillis(), 0, 100, 30, 0, false,
                MouseWheelEvent.WHEEL_UNIT_SCROLL, -20, -20);

        int value = zs.getValue();
        zs.mouseWheelMoved(wheelUp);
        assertTrue("scroll up reduce zoom " + value + " > " + zs.getValue()
                + " " + wheelUp.getPreciseWheelRotation(), value > zs
                .getValue());

        value = zs.getValue();
        zs.mouseWheelMoved(wheelDown);
        assertTrue("scroll down increase zoom " + value + " < " + zs.getValue()
                + " " + wheelDown.getPreciseWheelRotation(), value < zs
                .getValue());
    }

}

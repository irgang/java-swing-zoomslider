package de.fuhagen.sttp.demo;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

import de.fuhagen.sttp.gui.ZoomSlider;
import de.fuhagen.sttp.gui.ZoomSliderListener;

/**
 * Demo of {@link ZoomSlider} GUI element.
 * 
 * @author thomas
 *
 */
public class ZoomSliderDemo extends JFrame {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 683933609536453500L;

    /**
     * This method create a new {@link ZoomSlider} demo frame.
     * 
     * @param args
     */
    public static void main(String[] args) {
        new ZoomSliderDemo();
    }

    /**
     * {@link ZoomSlider} with buttons.
     */
    private ZoomSlider   zoomSliderButtons;
    /**
     * {@link ZoomSlider} without buttons.
     */
    private ZoomSlider   zoomSliderNoButtons;
    /**
     * Feedback label for {@link ZoomSlider} with buttons.
     */
    private final JLabel zoomLabel1;
    /**
     * Feedback label for {@link ZoomSlider} without buttons.
     */
    private final JLabel zoomLabel2;

    /**
     * This constructor builds a JFrame with two {@link ZoomSlider} elements and feedback labels.
     */
    public ZoomSliderDemo() {
        super("Zoom slider demo");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.LEFT));

        zoomLabel1 = new JLabel("Slider 1");
        zoomLabel2 = new JLabel("Slider 2");

        zoomSliderButtons = new ZoomSlider(250, 70, true);
        zoomSliderButtons.addZoomListener(new ZoomSliderListener() {

            @Override
            public void updateView(double zoom) {
                zoomLabel1.setText("Slider 1: " + zoom);
            }

            @Override
            public void executeAction() {
                zoomLabel1.setText(zoomLabel1.getText() + " action");
            }
        });

        zoomSliderNoButtons = new ZoomSlider(200, 40, false);
        zoomSliderNoButtons.addZoomListener(new ZoomSliderListener() {

            @Override
            public void updateView(double zoom) {
                zoomLabel2.setText("Slider 2: " + zoom);
            }

            @Override
            public void executeAction() {
                zoomLabel2.setText(zoomLabel2.getText() + " action");
            }
        });

        add(zoomSliderButtons);
        add(zoomLabel1);
        add(zoomSliderNoButtons);
        add(zoomLabel2);

        pack();
        setVisible(true);
    }
}

package de.fuhagen.sttp.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Rectangle2D;
import java.net.URL;
import java.util.Vector;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/**
 * The {@link ZoomSlider} is a GUI elements which shows the current zoom level
 * and allows to change this zoom level.
 * 
 * @author thomas
 *
 */
public class ZoomSlider extends JPanel implements MouseListener,
        MouseMotionListener, MouseWheelListener {

    /**
     * Serial version UID
     */
    private static final long          serialVersionUID  = 5395202940667614091L;

    /**
     * Constant for minimal zoom value.
     */
    private static final int           MIN_VAL           = 25;
    /**
     * Constant for maximal zoom value.
     */
    private static final int           MAX_VAL           = 400;
    /**
     * Constant for initial zoom value.
     */
    private static final int           INIT_VAL          = 100;

    /**
     * Size of this GUI element.
     */
    private Dimension                  size;
    /**
     * Current zoom value.
     */
    private int                        value;
    /**
     * Image icon for smaller button
     */
    private ImageIcon                  smaller;
    /**
     * Image icon for larger button.
     */
    private ImageIcon                  larger;
    /**
     * Cache for last drag point.
     */
    private Point                      dragPoint;
    /**
     * Show buttons flag.
     */
    private boolean                    buttons           = false;
    /**
     * Font for zoom level text, large.
     */
    private Font                       level0;
    /**
     * Font for zoom level text, medium.
     */
    private Font                       level1;
    /**
     * Font for zoom level text, small.
     */
    private Font                       level2;
    /**
     * Listeners for this zoom level GUI element.
     */
    private Vector<ZoomSliderListener> listeners;
    /**
     * This action is triggered with double click.
     */
    private Action                     doubleClickAction = null;
    /**
     * This action is triggered with left button.
     */
    private Action                     leftAction        = null;
    /**
     * This action is triggered with right button.
     */
    private Action                     rightAction       = null;
    /**
     * This action is triggered on zoom level change.
     */
    private Action                     updateAction      = null;

    /**
     * This constructor creates a new {@link ZoomSlider}.
     * 
     * @param width
     *      width of the slider
     * @param height
     *      height of the slider
     * @param showButtons
     *      true to show buttons.
     */
    public ZoomSlider(int width, int height, boolean showButtons) {
        super();

        listeners = new Vector<ZoomSliderListener>();

        buttons = showButtons;

        size = new Dimension(width, height);
        setSize(size);

        Border line = new LineBorder(Color.BLACK, 3, true);
        Border empty = new EmptyBorder(5, 5, 5, 5);
        Border compound = new CompoundBorder(line, empty);
        setBorder(compound);

        value = INIT_VAL;

        setLayout(null);

        if (buttons) {
            URL url = this.getClass().getClassLoader().getResource(
                    "de/fuhagen/sttp/resources/zoomSmaller.png");
            smaller = new ImageIcon(url);

            url = this.getClass().getClassLoader().getResource(
                    "de/fuhagen/sttp/resources/zoomLarger.png");
            larger = new ImageIcon(url);
        }

        addMouseListener(this);
        addMouseMotionListener(this);
        addMouseWheelListener(this);

        level0 = getFont();
        level0 = level0.deriveFont(20.0f);
        level0 = level0.deriveFont(Font.BOLD);

        level1 = getFont();
        level1 = level1.deriveFont(16.0f);
        level1 = level1.deriveFont(Font.PLAIN);

        level2 = getFont();
        level2 = level2.deriveFont(12.0f);
        level2 = level2.deriveFont(Font.PLAIN);

        setToolTipText("Zoom, double click for fit.");

    }

    public void setDoubleClickAction(Action action) {
        doubleClickAction = action;
    }

    public void setLeftAction(Action action) {
        leftAction = action;
    }

    public void setRightAction(Action action) {
        rightAction = action;
    }

    public void setUpdateAction(Action action) {
        updateAction = action;
    }

    /**
     * Add a new zoom listener to this slider.
     * 
     * @param listener
     *      listener as {@link ZoomSliderListener}
     */
    public void addZoomListener(ZoomSliderListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove a zoom listener to this slider.
     * 
     * @param listener
     *      listener as {@link ZoomSliderListener}
     */
    public void removeZoomListener(ZoomSliderListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void paintComponent(final Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        super.paintComponent(g);

        double width0;
        double width1;
        double width2;

        double x0;
        double x1l;
        double x1r;
        double x2l;
        double x2r;

        double y0;
        double y1;
        double y2;

        int distance = 2;

        Rectangle2D bounds = getFontMetrics(level0)
                .getStringBounds("888%", g2d);
        width0 = bounds.getWidth();
        x0 = (getWidth() - width0) / 2.0;
        y0 = ((getHeight() - bounds.getHeight()) / 2.0)
                + getFontMetrics(level0).getAscent();

        bounds = getFontMetrics(level1).getStringBounds("888", g2d);
        width1 = bounds.getWidth();
        x1l = x0 - bounds.getWidth() - distance;
        x1r = x0 + width0 + distance;
        y1 = ((getHeight() - bounds.getHeight()) / 2.0)
                + getFontMetrics(level1).getAscent();

        bounds = getFontMetrics(level2).getStringBounds("888", g2d);
        width2 = bounds.getWidth();
        x2l = x1l - bounds.getWidth() - distance;
        x2r = x1r + width1 + distance;
        y2 = ((getHeight() - bounds.getHeight()) / 2.0)
                + getFontMetrics(level2).getAscent();

        String textCenter = value + "%";
        String textLeft1 = (value - 5) + "";
        if (value - 5 < MIN_VAL) {
            textLeft1 = "";
        }
        String textLeft2 = (value - 10) + "";
        if (value - 10 < MIN_VAL) {
            textLeft2 = "";
        }
        String textRight1 = (value + 5) + "";
        if (value + 5 > MAX_VAL) {
            textRight1 = "";
        }
        String textRight2 = (value + 10) + "";
        if (value + 10 > MAX_VAL) {
            textRight2 = "";
        }

        g2d.setFont(level0);

        bounds = getFontMetrics(level0).getStringBounds(textCenter, g2d);
        double offX = x0 + ((width0 - bounds.getWidth()) / 2.0);
        g2d.drawString(textCenter, (int) offX, (int) y0);

        g2d.setFont(level1);
        g2d.setColor(Color.GRAY);

        bounds = getFontMetrics(level1).getStringBounds(textLeft1, g2d);
        offX = x1l + ((width1 - bounds.getWidth()) / 2.0);
        g2d.drawString(textLeft1, (int) offX, (int) y1);

        bounds = getFontMetrics(level1).getStringBounds(textRight1, g2d);
        offX = x1r + ((width1 - bounds.getWidth()) / 2.0);
        g2d.drawString(textRight1, (int) offX, (int) y1);

        g2d.setFont(level2);
        g2d.setColor(Color.LIGHT_GRAY);

        bounds = getFontMetrics(level2).getStringBounds(textLeft2, g2d);
        offX = x2l + ((width2 - bounds.getWidth()) / 2.0);
        g2d.drawString(textLeft2, (int) offX, (int) y2);

        bounds = getFontMetrics(level2).getStringBounds(textRight2, g2d);
        offX = x2r + ((width2 - bounds.getWidth()) / 2.0);
        g2d.drawString(textRight2, (int) offX, (int) y2);

        if (buttons) {
            int offsetY = (getHeight() - smaller.getIconHeight()) / 2;
            smaller.paintIcon(this, g2d, 0, offsetY);

            int offsetX = (getWidth() - smaller.getIconWidth());
            larger.paintIcon(this, g2d, offsetX, offsetY);
        }
    }

    /**
     * This method can be used to update the zoom value of this slider.
     * 
     * @param newValue
     *         zoom level percent as int
     */
    public void setValue(int newValue) {
        if (newValue < MIN_VAL) {
            value = MIN_VAL;
        } else if (newValue > MAX_VAL) {
            value = MAX_VAL;
        } else {
            value = newValue;
        }
        updateListeners();
        invalidate();
        repaint();
    }

    /**
     * This method returns the zoom level as percent int value.
     * 
     * @return
     *      zoom level as int
     */
    public int getValue() {
        return value;
    }

    @Override
    public Dimension getPreferredSize() {
        return size;
    }

    @Override
    public Dimension getMinimumSize() {
        return size;
    }

    @Override
    public Dimension getMaximumSize() {
        return size;
    }

    /**
     * This method implements the drag zoom feature.
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        if (dragPoint != null) {
            int delta = e.getPoint().x - dragPoint.x;
            setValue(getValue() + delta);
        }
        dragPoint = e.getPoint();
        updateListeners();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    /**
     * This method implements the zoom with buttons if the buttons are enabled. Even if no buttons are enabled
     * a double click calls the centerView and fitView methods of all listeners.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 1 && buttons) {
            if (e.getX() >= getWidth() / 2) {
                setValue(getValue() + 25);
                if (rightAction != null) {
                    if (rightAction.isEnabled()) {
                        ActionEvent event = new ActionEvent(this, 0,
                                "right button", System.currentTimeMillis(), 0);
                        rightAction.actionPerformed(event);
                    }
                }
            } else {
                setValue(getValue() - 25);
                if (leftAction != null) {
                    if (leftAction.isEnabled()) {
                        ActionEvent event = new ActionEvent(this, 0,
                                "left button", System.currentTimeMillis(), 0);
                        leftAction.actionPerformed(event);
                    }
                }
            }
        } else if (e.getClickCount() >= 2) {
            for (ZoomSliderListener listener : listeners) {
                listener.executeAction();
            }
            if (doubleClickAction != null) {
                if (doubleClickAction.isEnabled()) {
                    ActionEvent event = new ActionEvent(this, 0,
                            "double click", System.currentTimeMillis(), 0);
                    doubleClickAction.actionPerformed(event);
                }
            }
        }
    }

    /**
     * This method updates the zoom levels of the zoom slider listeners.
     */
    private void updateListeners() {

        double zoomValue = ((double) getValue()) / 100.0;

        for (ZoomSliderListener listener : listeners) {
            listener.updateView(zoomValue);
        }

        if (updateAction != null) {
            if (updateAction.isEnabled()) {
                ActionEvent event = new ActionEvent(this, 0, Double
                        .toString(zoomValue), System.currentTimeMillis(), 0);
                updateAction.actionPerformed(event);
            }
        }
    }

    /**
     * This method is used to initialize the drag point cache.
     */
    @Override
    public void mousePressed(MouseEvent e) {
        dragPoint = e.getPoint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    /**
     * This method implements the scroll zoom feature.
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        double rotation = e.getPreciseWheelRotation();
        rotation = rotation * 20;
        setValue(getValue() + ((int) rotation));
    }
}

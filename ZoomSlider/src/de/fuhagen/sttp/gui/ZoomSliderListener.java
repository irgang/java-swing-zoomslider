package de.fuhagen.sttp.gui;

/**
 * Listener interface for {@link ZoomSlider} events.
 * 
 * @author thomas
 *
 */
public interface ZoomSliderListener {

    /**
     * This method is called if the zoom value is updated.
     * 
     * @param zoom
     *      zoom as double value.
     */
    public void updateView(double zoom);

    /**
     * This method is triggered with double click.
     */
    public void executeAction();
}

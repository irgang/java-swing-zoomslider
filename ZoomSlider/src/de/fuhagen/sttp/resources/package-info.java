/**
 * This package contains the resources for GUI elements.
 * 
 * @author thomas
 *
 */
package de.fuhagen.sttp.resources;
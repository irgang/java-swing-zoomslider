java_swing_ZoomSlider
=====================

A nice slider to select a zoom level.

![screenshot](ZoomSlider.png)
